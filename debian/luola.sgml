<!doctype refentry PUBLIC "-//OASIS//DTD DocBook V4.1//EN" [

<!-- Process this file with docbook-to-man to generate an nroff manual
     page: `docbook-to-man manpage.sgml > manpage.1'.  You may view
     the manual page with: `docbook-to-man manpage.sgml | nroff -man |
     less'.  A typical entry in a Makefile or Makefile.am is:

manpage.1: manpage.sgml
	docbook-to-man $< > $@
  -->

  <!-- Fill in your name for FIRSTNAME and SURNAME. -->
  <!ENTITY dhfirstname "<firstname>Christian T.</firstname>">
  <!ENTITY dhsurname   "<surname>STEIGIES</surname>">
  <!-- Please adjust the date whenever revising the manpage. -->
  <!ENTITY dhdate      "<date>July 25, 2005</date>">
  <!-- SECTION should be 1-8, maybe w/ subsection other parameters are
       allowed: see man(7), man(1). -->
  <!ENTITY dhsection   "<manvolnum>6</manvolnum>">
  <!ENTITY dhemail     "<email>cts@debian.org</email>">
  <!ENTITY dhusername  "Christian T. Steigies">
  <!ENTITY dhucpackage "<refentrytitle>LUOLA</refentrytitle>">
  <!ENTITY dhpackage   "luola">

  <!ENTITY debian      "<productname>Debian GNU/Linux</productname>">
  <!ENTITY gnu         "<acronym>GNU</acronym>">
]>

<refentry>
  <refentryinfo>
    <address>
      &dhemail;
    </address>
    <author>
      &dhfirstname;
      &dhsurname;
    </author>
    <copyright>
      <year>2002</year>
      <holder>&dhusername;</holder>
    </copyright>
    &dhdate;
  </refentryinfo>
  <refmeta>
    &dhucpackage;

    &dhsection;
  </refmeta>
  <refnamediv>
    <refname>&dhpackage;</refname>

    <refpurpose>a 2D arcade game where you fly a small V shaped ship
 in different kinds of levels.</refpurpose>
  </refnamediv>
  <refsynopsisdiv>
    <cmdsynopsis>
      <command>&dhpackage;</command>
    </cmdsynopsis>
  </refsynopsisdiv>
  <refsect1>
    <title>DESCRIPTION</title>

    <para>This manual page documents briefly the
      <command>&dhpackage;</command> command.</para>

    <para>This manual page was written for the &debian; distribution
      because the original program does not have a manual page.
      Please read the full documentation at: /usr/share/doc/luola/README.gz</para>

  </refsect1>
  <refsect1>
    <title>OPTIONS</title>

    <para>These programs follow the usual GNU command line syntax,
      with long options starting with two dashes (`-').  A summary of
      options is included below.  For a complete description, see the
      <application>/usr/share/doc/luola/README.gz</application> file.</para>

    <variablelist>
      <varlistentry>
        <term><option>--fullscreen</option>
        </term>
        <listitem>
          <para>Play the game in fullscreen mode.</para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term><option>--window</option>
        </term>
        <listitem>
          <para>Play the game in windowed mode.</para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term><option>--hidemouse</option>
        </term>
        <listitem>
          <para>This is very useful. It hides the mouse pointer when it is over the screen.</para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term><option>--showmouse</option>
        </term>
        <listitem>
          <para>Show the mouse pointer when it is over the window.</para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term><option>--pad</option>
        </term>
        <listitem>
          <para>Enables the joystick/pad support.</para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term><option>--nopad</option>
        </term>
        <listitem>
          <para>Disables the joystick/pad support.</para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term><option>--sounds</option>
        </term>
        <listitem>
          <para>Enable the sound system.</para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term><option>--nosounds</option>
        </term>
        <listitem>
          <para>Disable the sounds.</para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term><option>--lang 'lang'</option>
        </term>
        <listitem>
          <para>Use language specified by 'lang'. Eg. '--lang fi' uses the Finnish translation.</para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term><option>--audiorate</option>
        </term>
        <listitem>
          <para>Set audio sampling frequency.</para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term><option>--audiochannels</option>
        </term>
        <listitem>
          <para>Set audio channels (1 or 2).</para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term><option>--audiochunks</option>
        </term>
        <listitem>
          <para>Set audio chunks.</para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term><option>--help</option>
        </term>
        <listitem>
          <para>Show help message.</para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term><option>--version</option>
        </term>
        <listitem>
          <para>Show version information.</para>
        </listitem>
      </varlistentry>
 
    </variablelist>
  </refsect1>
  <refsect1>
    <title>AUTHOR</title>

    <para>This manual page was written by &dhusername; &dhemail; for
      the &debian; system (but may be used by others).  Permission is
      granted to copy, distribute and/or modify this document under
      the terms of the <acronym>GNU</acronym> Free Documentation
      License, Version 1.1 or any later version published by the Free
      Software Foundation; with no Invariant Sections, no Front-Cover
      Texts and no Back-Cover Texts.</para>

  </refsect1>
</refentry>

<!-- Keep this comment at the end of the file
Local variables:
mode: sgml
sgml-omittag:t
sgml-shorttag:t
sgml-minimize-attributes:nil
sgml-always-quote-attributes:t
sgml-indent-step:2
sgml-indent-data:t
sgml-parent-document:nil
sgml-default-dtd-file:nil
sgml-exposed-tags:nil
sgml-local-catalogs:nil
sgml-local-ecat-files:nil
End:
-->
